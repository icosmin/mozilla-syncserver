FROM debian:stable
MAINTAINER Cosmin Ioiart <cosmin@ioiart.eu>
ENV container docker

# Base system setup

RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && apt-get install --no-install-recommends -y \
    vim locales \
    && apt-get clean; \
    locale-gen C.UTF-8 && LANG=C.UTF-8 /usr/sbin/update-locale; \
    useradd --create-home app; \
    mkdir /config; \
    mkdir /data; \

# Build the Sync server

DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    ca-certificates \
    python-virtualenv \
    && apt-get clean; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    chown app:app /data /config


ENV LANG C.UTF-8
USER app
ADD ./syncserver.tar /home/app/
ADD ./syncserver.ini /config/syncserver.ini
WORKDIR /home/app

# Run the Sync server
VOLUME /data /config
EXPOSE 5000
ENTRYPOINT ["/home/app/local/bin/pserve"]
CMD ["/config/syncserver.ini"]
